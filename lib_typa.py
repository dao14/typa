#!/usr/bin/env python3


import curses


class typa_tv :

    def __init__( self, stdscr ) :
        self.screen = stdscr
        self.screen.keypad( True )

        curses.noecho()
        curses.cbreak()
        curses.init_pair( 1, curses.COLOR_RED, curses.COLOR_BLACK )

        self.center_y = int( 0.5 * curses.LINES )
        self.center_x = int( 0.5 * curses.COLS )

    def clear( self ) :
        self.screen.clear()

    def refresh( self ) :
        self.screen.refresh()

    def input( self ) :
        return self.screen.getkey()

    def output_main( self, character_data ) :
        x_offset = int( 0.5 * len( character_data.keys() ) )
        for idx, values in character_data.items() :

            ch = values[ 0 ]
            status = values[ 1 ]

            if status == 0 :
                self.screen.addstr( self.center_y, self.center_x - x_offset + idx,
                ch )
            elif status == 1 : 
                self.screen.addstr( self.center_y, self.center_x - x_offset + idx,
                ch, curses.A_BOLD )
            elif status == 2 :
                self.screen.addstr( self.center_y, self.center_x - x_offset + idx,
                ch, curses.color_pair( 1 ) )

    def output_main_string( self, output ) :
        x_offset = int( 0.5 * len( list( output ) ) )
        self.screen.addstr( self.center_y, self.center_x - x_offset, output )

    def __del__( self ) :
        self.screen.keypad( False )

        curses.nocbreak()
        curses.echo()

        curses.endwin()


class typa_characters :

    def __init__( self, WORDSLIST_PATH ) :
        self.get_words( WORDSLIST_PATH )  #  Initilize self.words
        self.characters = []

    def get_words( self, WORDSLIST_PATH ) :
        self.words = []
        with open( WORDSLIST_PATH ) as f :
            self.words = f.read().split( "\n" )
            self.words.pop( -1 )

    def word_to_character( self, word, SPACE ) :
        characters = list( word )
        if SPACE :
            characters.append( " " )
        return characters

    def characters_to_dictionary( self, characters ) :
        character_data = {}
        for idx, ch in enumerate( characters ) :
            character_data[ idx ] = [ ch, 0 ]
        return character_data
